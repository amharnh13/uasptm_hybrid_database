import { Component, OnInit } from '@angular/core';
import { CrudService } from './../service/crud.service';
 
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {
  mhs: any;
  mhsName: string;
  mhsAge: number;
  mhsAddress: string;
 
  constructor(private crudService: CrudService) { }
 
  ngOnInit() {
    this.crudService.read_mhs2017().subscribe(data => {
      this.mhs = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          Name: e.payload.doc.data()['Name'],
          Age: e.payload.doc.data()['Age'],
          Address: e.payload.doc.data()['Address'],
        };
      })
      console.log(this.mhs);
    });
  }
 
  CreateRecord() {
    let record = {};
    record['Name'] = this.mhsName;
    record['Age'] = this.mhsAge;
    record['Address'] = this.mhsAddress;
    this.crudService.create_mhs2017(record).then(resp => {
      this.mhsName = "";
      this.mhsAge = undefined;
      this.mhsAddress = "";
      console.log(resp);
    })
      .catch(error => {
        console.log(error);
      });
  }
 
  RemoveRecord(rowID) {
    this.crudService.delete_mhs2017(rowID);
  }
 
  EditRecord(record) {
    record.isEdit = true;
    record.EditName = record.Name;
    record.EditAge = record.Age;
    record.EditAddress = record.Address;
  }
 
  UpdateRecord(recordRow) {
    let record = {};
    record['Name'] = recordRow.EditName;
    record['Age'] = recordRow.EditAge;
    record['Address'] = recordRow.EditAddress;
    this.crudService.update_mhs2017(recordRow.id, record);
    recordRow.isEdit = false;
  }
}