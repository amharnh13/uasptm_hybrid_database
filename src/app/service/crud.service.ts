import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
 
@Injectable({
  providedIn: 'root'
})
export class CrudService {
 
  constructor(
    private firestore: AngularFirestore
  ) { }
 
 
  create_mhs2017(record) {
    return this.firestore.collection('mhs2017').add(record);
  }
 
  read_mhs2017() {
    return this.firestore.collection('mhs2017').snapshotChanges();
  }
 
  update_mhs2017(recordID,record){
    this.firestore.doc('mhs2017/' + recordID).update(record);
  }
 
  delete_mhs2017(record_id) {
    this.firestore.doc('mhs2017/' + record_id).delete();
  }
}