// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyAiwXfqqjrmrZ5iXiLoJxpdohCnk_D-cv4",
    authDomain: "mahasiswa-2017.firebaseapp.com",
    databaseURL: "https://mahasiswa-2017.firebaseio.com",
    projectId: "mahasiswa-2017",
    storageBucket: "",
    messagingSenderId: "656578208088",
    appId: "1:656578208088:web:bd6cf2efec372db0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
